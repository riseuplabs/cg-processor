class LogEntry < ActiveRecord::Base

  # different flags for the log entries:
  DEFAULT = 0
  ERROR = 1
  STATE = 2

  belongs_to :job

  named_scope :error, {:conditions => {:flag => 1}}

  named_scope :stdout, {:conditions => {:flag => 0}}

end
