class JobsController < ApplicationController

  # GET /jobs
  def index
    # this would return an array of ids instead of a an array of objects
    #@jobs.collect!{ |asset| asset.id }
    # this would return a hash of object-like hashes with just the ids that activerecord could consume
    #@jobs.collect!{ |asset| {:job => {:id => asset.id}} }
    #
    @jobs = Job.with_state(params[:state])
    respond_to do |format|
      format.html
      format.xml  { render :xml => @jobs }
      format.json { render :json => @jobs }
    end
  end

  # GET /jobs/1
  # GET /jobs/1.xml
  def show
    @job = Job.find(params[:id])
    respond_to do |format|
      format.html
      format.xml  { render :xml => @job }
      format.json { render :json => @job }
      format.any { send_file @job.output_file }
    end
  end

  # GET /jobs/new
  # GET /jobs/new.xml
  def new
    @job = Job.new
    respond_to do |format|
      format.html
      format.xml  { render :xml => @job }
      format.json { render :json => @job }
    end
  end

  # GET /jobs/1/edit
  def edit
    @job = Job.find(params[:id])
  end

  # POST /jobs
  # POST /jobs.xml
  def create
    @job = Job.new(params[:job])
    respond_to do |format|
      if @job.save
        @job.save_upload_to_file(params[:data])
        format.html { redirect_to(@job, :notice => 'Job was successfully created.') }
        format.xml  { render :xml => @job, :status => :created, :location => @job }
        format.json { render :json => {:result => 1, :id => @job.id}.to_json }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @asset.errors, :status => :unprocessable_entity }
        format.json { render :json => {:result => 0, :errors => @job.errors}.to_json }
      end
    end
  end


  # PUT /jobs/1
  # PUT /jobs/1.xml
  def update
    @job = Job.find(params[:id])
    respond_to do |format|
      if @job.update_attributes(params[:job])
        format.html { redirect_to(@job, :notice => 'Job was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @job.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /jobs/1
  # DELETE /jobs/1.xml
  def destroy
    @job = Job.find(params[:id])
    @job.destroy
    respond_to do |format|
      format.html { redirect_to(jobs_url) }
      format.xml  { head :ok }
    end
  end
end

