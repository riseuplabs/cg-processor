
unless defined?(PYTHON_COMMAND)
  PYTHON_COMMAND = `which python`.chomp
end

DISMOD_COMMAND = File.dirname(__FILE__) + '/test.py'

class DismodTransmogrifier < Media::Transmogrifier

  def input_types
    %w( application/dismod-model )
  end

  def output_types
    %w( application/dismod-result )
  end

  def available?
    command_available?(PYTHON_COMMAND)
  end

  def run(&block)
    arguments = [PYTHON_COMMAND, DISMOD_COMMAND]
    arguments << input_file << output_file
    run_command(*arguments, &block)
  end

end

DismodTransmogrifier.new

