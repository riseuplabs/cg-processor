
config.cache_classes                                 = true
config.whiny_nils                                    = true
config.action_controller.consider_all_requests_local = true
config.action_controller.perform_caching             = false
config.action_view.cache_template_loading            = true
config.action_controller.allow_forgery_protection    = false
config.action_mailer.delivery_method                 = :test

#
# For some reason, Spawn doesn't seem to be working in test env,
# so for now I am setting the method in test to yield.
#
Spawn::default_options(:method => :yield)
