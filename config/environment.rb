
RAILS_GEM_VERSION = '2.3.11' unless defined? RAILS_GEM_VERSION

require File.join(File.dirname(__FILE__), 'boot')

Rails::Initializer.run do |config|
  # Skip some frameworks
  config.frameworks -= [ :active_resource, :action_mailer ]

  config.time_zone = 'UTC'

  config.plugin_paths << [Rails.root, 'transmogrifiers'].join('/')
end

