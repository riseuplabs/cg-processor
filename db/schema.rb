# This file is auto-generated from the current state of the database. Instead of editing this file, 
# please use the migrations feature of Active Record to incrementally modify your database, and
# then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your database schema. If you need
# to create the application database on another system, you should be using db:schema:load, not running
# all the migrations from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20110313195057) do

  create_table "jobs", :force => true do |t|
    t.datetime "created_at"
    t.datetime "finished_at"
    t.string   "state",                :limit => 10, :default => "new"
    t.string   "status",               :limit => 10
    t.string   "type"
    t.string   "input_type"
    t.string   "input_url"
    t.string   "input_file"
    t.binary   "input_data"
    t.string   "output_type"
    t.string   "output_url"
    t.string   "output_file"
    t.binary   "output_data"
    t.text     "options"
    t.string   "failed_callback_url"
    t.string   "success_callback_url"
  end

  create_table "log_entries", :force => true do |t|
    t.integer  "job_id"
    t.string   "text"
    t.integer  "progress"
    t.integer  "flag",       :default => 0
    t.datetime "created_at"
  end

  add_index "log_entries", ["job_id"], :name => "index_log_entries_on_job_id"

end
