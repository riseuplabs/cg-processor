
class CreateLogEntries < ActiveRecord::Migration
  def self.up
    create_table :log_entries do |t|
      t.integer  :job_id
      t.string   :text
      t.integer  :progress
      t.integer  :flag, :default => 0
      t.datetime :created_at
    end
    add_index :log_entries, :job_id
  end

  def self.down
    drop_table :log_entries
  end
end
