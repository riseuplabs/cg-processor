class CreateJobs < ActiveRecord::Migration
  def self.up
    create_table :jobs do |t|
      t.datetime "created_at"
      t.datetime "finished_at"

      t.string   "state", :default => 'new', :limit => 10
      t.string   "status", :limit => 10
      t.string   "type"

      t.string   "input_type"
      t.string   "input_url"
      t.string   "input_file"
      t.binary   "input_data"

      t.string   "output_type"
      t.string   "output_url"
      t.string   "output_file"
      t.binary   "output_data"

      t.text     "options"

      t.string   "failed_callback_url"
      t.string   "success_callback_url"
    end
  end

  def self.down
    drop_table :jobs
  end
end
